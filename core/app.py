from flask_sqlalchemy import SQLAlchemy
from flask import Flask
from flask_restful import Api
import os
from dotenv import load_dotenv
from werkzeug.http import HTTP_STATUS_CODES
from werkzeug.exceptions import HTTPException

load_dotenv()

class ExtendedAPI(Api):
    def handle_error(self, err):
        print(err)
        if isinstance(err, HTTPException):
            return jsonify({
                    'message': getattr(
                        err, 'description', HTTP_STATUS_CODES.get(err.code, '')
                    )
                }), err.code
        if not getattr(err, 'message', None):
            return jsonify({
                'message': 'Server has encountered some error'
                }), 500
        return jsonify(**err.kwargs), err.http_status_code


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = f"mysql+pymysql://{os.environ['DBUSER']}:{os.environ['PASSWORD']}@{os.environ['HOST']}/{os.environ['DATABASE']}"
db = SQLAlchemy(app)
api = ExtendedAPI(app)


from core.views import WebsiteView

api.add_resource(WebsiteView, 
    '/',
    '/<string:name>'
    )