from core.models import WebContent
from core.app import db
from typing import Any
def get_content_by_name(name: str):
	web_content = WebContent.query.filter_by(name=name)
	web_content_row= web_content.first()
	if web_content_row:
		return web_content_row.serialize
	else:
		return None


def get_all_contents():
	all_contents = WebContent.query.all()
	if len(all_contents) > 0:
		return all_contents
	else:
		return None
	
def insert_website_content(name: str, content: str):
	content = WebContent.create(name=name,content=content)
	return content.serialize


def delete_content_by_name(name: str):
	web_content = WebContent.query.filter_by(name=name)
	web_content_row= web_content.first()
	if web_content_row:
		web_content.delete()
		db.session.commit()


def update_content_by_name(name: str, data: Any):
	web_content = WebContent.query.filter_by(name=name)
	web_content_row= web_content.first()
	if web_content_row:
		WebContent.update(web_content_row,**data)