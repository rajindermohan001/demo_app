from core.app import db

class WebContent(db.Model):
    name = db.Column(db.String(100), unique=True, nullable=False, primary_key=True)
    content = db.Column(db.Text, nullable=False)

    @staticmethod    
    def create(**kwargs):
        website = WebContent(**kwargs)
        db.session.add(website)
        db.session.commit()
        return website

    @staticmethod
    def update(obj,**kwargs):
        name = kwargs.get('name')
        content = kwargs.get('content')
        if name:
            obj.name=name
        if content:
            obj.content = content
        db.session.commit()

    @property
    def serialize(self):
       return {
           'name': self.name,
           'content'  : self.content
       }

db.create_all()