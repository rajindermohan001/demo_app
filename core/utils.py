from collections import Counter
from flask_restful import reqparse
from nltk.tokenize import RegexpTokenizer


def get_request_parser_for_query_param():
	parser = reqparse.RequestParser()
	parser.add_argument('name', type=str)
	parser.add_argument('num', type=int)
	return parser.parse_args()

def get_request_parser_for_post_data():
	parser = reqparse.RequestParser()
	parser.add_argument('name', type=str, required=True)
	parser.add_argument('content', type=str, required=True)
	return parser.parse_args()


def get_common_words_for_number(data,n:int):
	tokenizer = RegexpTokenizer(r'\w+')
	tokens = tokenizer.tokenize(data)
	tokens_list = list(map(lambda a: a.lower(),tokens))
	word_count_dict = Counter(tokens)
	return dict(word_count_dict.most_common(n))