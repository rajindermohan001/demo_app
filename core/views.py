from flask import jsonify, request
from flask_restful import Resource,abort, reqparse, fields
from core.models import WebContent
from core.utils import get_common_words_for_number, get_request_parser_for_query_param, get_request_parser_for_post_data
from core.app import db
from core import dal
from core.errors import ValidationError as VE

class WebsiteView(Resource):

    def get(self):
        arg = get_request_parser_for_query_param()
        if not any([arg["name"],arg["num"]]):
            return {"msg":"name and num both can't be blank."}, 404

        elif arg["name"] and arg["num"]:
            wbsite_content = dal.get_content_by_name(arg["name"])
            if wbsite_content:
                common_words = get_common_words_for_number(wbsite_content['content'],arg["num"])
                return {"data": common_words}, 200
            else:
                return {"msg":"website name not found."}, 404

        elif arg["name"]:
            wbsite_content = dal.get_content_by_name(arg["name"])
            if wbsite_content:
                return {"data": wbsite_content}, 200
            else:
                return {"msg":"website name not found."}, 404

        else:
            data = dal.get_all_contents()
            if data:
                content_list = [i.content for i in data]
                all_content = " ".join(content_list)
                common_words = get_common_words_for_number(all_content,arg["num"])
                return {"data": common_words}, 200
            else:
                return {"msg":"No data is available."}, 404

    def post(self,*args,**kwargs):
        arg = get_request_parser_for_post_data()
        wbsite_content = dal.get_content_by_name(arg["name"])
        if wbsite_content:
            return {"msg":"Website already exits."}, 409
        else:
            wbsite_content = dal.insert_website_content(name=arg["name"],content=arg["content"])
            return {"data": wbsite_content}, 201

    def delete(self,name):
        dal.delete_content_by_name(name=name)
        return {},204
